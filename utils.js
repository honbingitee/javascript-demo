/*
 * @Author: hongbin
 * @Date: 2021-12-06 18:13:40
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-06 18:28:43
 * @Description: 常用js函数
 */

/**
 * @description: 随机rgba格式颜色
 * @return {string} rgba
 */
function randomRGBA() {
    const r = floor(random() * 250);
    const g = floor(random() * 250);
    const b = floor(random() * 250);
    const a = random();
    return `rgba(${r},${g},${b},${a})`;
}

/**
 * @description: 随机十六进制
 * @return {string} #xxxxxx
 */
function randomColor() {
    // .padEnd 有时候会出现不满6位的十六进制颜色canvas会报错，使用padEnd填充
    return "#" + Math.round(Math.random() * 0xffffff).toString(16).padEnd(6, "9");
}

/**
 * @description: 随机十六进制透明色
 * @return {string} #xxxxxxxx
 */
function randomTransparentColor() {
    // .padEnd(8, "9") 有时候会出现不满8位的十六进制颜色canvas会报错，使用padEnd填充满8位
    return "#" + Math.round(Math.random() * 0xffffffff).toString(16).padEnd(8, "9");
}