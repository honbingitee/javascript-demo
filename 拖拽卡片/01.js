/**
 * @description: 开启拖拽
 * @param {*} selectors 容器选择器
 */
function addDrag(selectors) {
    const containers = document.querySelectorAll(selectors);
    const angle = 15;

    for (const container of containers) {
        container.addEventListener('mouseenter', (e) => {
            e.target.style.transition = '';
            e.target.style['will-change'] = "transform";
        })
        container.addEventListener('mousemove', ({ pageX, pageY }) => {
            // 设置获取card中心点坐标
            const { offsetWidth, offsetHeight, offsetLeft, offsetTop } = container;
            const halfW = offsetWidth / 2;
            const halfH = offsetHeight / 2;
            const centerX = offsetLeft + halfW;
            const centerY = offsetTop + halfH;
            const dirX = pageX - centerX;
            const dirY = pageY - centerY;

            container.style.transform = `perspective(1000px)  rotateX(${dirX / halfW * angle}deg) rotateY(${dirY / halfH * angle}deg)`;
        })
        container.addEventListener('mouseleave', (e) => {
            // 复原
            e.target.style.transform = '';
            e.target.style.transition = 'transform 0.3s linear';
        })
    }
}
addDrag(".card", 'span');