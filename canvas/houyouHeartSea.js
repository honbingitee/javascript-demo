/*
 * @Author: hongbin
 * @Date: 2021-12-06 21:59:14
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-07 08:50:21
 * @Description:猴油脚本的心海插件
 */

// 添加元素

const style = document.createElement("style");
style.innerHTML = `
#heartSea,#backCanvas{
    margin: 0;
    position: absolute;
    top: 0;
    left: 0;
}
#heartSea {
    transition: opacity 0.3s linear;
    z-index: -99;
}

#backCanvas {
    z-index: -100;
}
`;
document.head.appendChild(style);

const { offsetWidth, offsetHeight } = document.body;

const backCanvas = document.createElement('canvas');
backCanvas.id = 'backCanvas';
const backCtx = backCanvas.getContext("2d");
document.body.appendChild(backCanvas);
const canvas = document.createElement('canvas');
document.body.appendChild(canvas);
canvas.id = 'heartSea';

const ctx = canvas.getContext("2d");
const { PI, cos, pow, sin, floor, random } = Math;

canvas.width = offsetWidth;
canvas.height = offsetHeight;
//绘制 背景
backCanvas.width = offsetWidth;
backCanvas.height = offsetHeight;

gridLine(backCtx, offsetWidth, offsetHeight, 250, 300);
technologyLine(backCtx, offsetHeight, 40);

const accuracy = 100; //渲染精度

const builtInLinearGradient = [
    ["#D8DC71", "#65D3CA"], ["#DF5675", "#4762ED"], ["#5BC9F4", "#A8F8A8"],
    ["#C64A69", "#D1AA5F"], ["#988EBF", "#4B3997"], ["#C79FAE", "#DE4477"], ["#CFAA9E", "#CD7232"], ['#96AE92', '#354F73'], ["#313573", "#6F7AA5"], ["#E7BDAA", "#524C8A"], ["#9F44B7", "#BA8BA2"], ["#C4446A", "#C5A15A"]
]

/**
 * @description: 心形
 * @param {*} x x轴
 * @param {*} y y轴
 * @param {*} r 半径
 * @param {*} speed 速度
 * @return {Heart} new Heart
 */
function Heart(x, y, r, speed = 3) {
    this.r = r;
    this.x = x;
    this.y = y;
    this.speed = speed;

    const linearGradient = ctx.createLinearGradient(0, r * -10, r * 20, r * 20);
    const color = builtInLinearGradient[floor(random() * builtInLinearGradient.length)];
    const opacity = Math.round(Math.random() * 0xff).toString(16).padEnd(2, 5);
    linearGradient.addColorStop(0, `${color[0]}${opacity}`);
    linearGradient.addColorStop(1, `${color[1]}${opacity}`);
    // TODO 提供一些好看的渐变配色
    this.fillStyle = linearGradient;
    const point = [];
    for (let i = 0; i < accuracy; i++) {
        const step = (i / accuracy) * (PI * 2);
        const xx = r * (16 * pow(sin(step), 3));
        const yy = r * (13 * cos(step) - 5 * cos(2 * step) - 2 * cos(3 * step) - cos(4 * step));
        point.push(xx, yy);
    };
    this.point = point;
    this.draw();
}

/**
 * @description: 根据参数 绘制Heart
 */
Heart.prototype.draw = function () {
    ctx.save(); //保存 rotate translate 状态
    ctx.beginPath();
    ctx.translate(this.x, this.y);
    ctx.rotate(PI);
    const point = this.point;

    for (let i = 0; i < point.length; i += 2) {
        ctx.lineTo(point[i], point[i + 1]);
    }

    ctx.fillStyle = this.fillStyle;
    ctx.strokeStyle = this.fillStyle;
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.restore(); // 释放状态
}

/**
 * @description:  向上移动
 */
Heart.prototype.moveUp = function () {
    this.y -= this.speed;
    this.draw();
}

const heartLength = 20;
const heartList = [];

for (let i = 0; i < heartLength; i++) {
    const x = random() * (offsetWidth - 200) + 100;
    const y = random() * offsetHeight + offsetHeight;
    const r = random() * 4 + 3;
    const speed = r;
    heartList.push(new Heart(x, y, r, speed));
}

function moveUp() {
    ctx.clearRect(0, 0, offsetWidth, offsetHeight);
    let overItemLength = 0;//已经在屏幕外边的❤️

    for (const heart of heartList) {
        //! 直接删除超出屏幕的元素会导致页面闪灼
        if (heart.y > -heart.r * 20) {
            heart.moveUp();
        } else overItemLength++;
    }

    overItemLength != heartList.length && setTimeout(moveUp, 16)
    if (overItemLength == heartList.length) {
        console.log("over");
    }
};

moveUp();

/**
 * @description: 垂直水平网格线
 * @param {CanvasRenderingContext2D} ctx canvasContext
 * @param {number} w 宽度
 * @param {number} h 高度
 * @param {number} wSpace x轴间距
 * @param {number} ySpace y轴间距
 * @return {void}
 */
function gridLine(ctx, w, h, wSpace = 100, ySpace = 100) {
    for (let i = 0; i < w; i += wSpace) {
        ctx.beginPath();
        ctx.moveTo(i, 0);
        ctx.lineTo(i, h);
        ctx.strokeStyle = "#ddd";
        ctx.stroke();
        ctx.closePath();
    }

    for (let i = 0; i < h; i += ySpace) {
        ctx.beginPath();
        ctx.moveTo(0, i);
        ctx.lineTo(w, i);
        ctx.strokeStyle = "#ddd";
        ctx.stroke();
        ctx.closePath();
    }
}

/**
 * @description: 科技风线条
 * @param {CanvasRenderingContext2D} ctx canvasContext
 * @param {number} h 高度
 * @param {number} space 间距
 * @param {string} lineColor 线条颜色
 * @return {void} 
 */
function technologyLine(ctx, h, space, lineColor = "#ccc") {
    for (let i = 0; i < h; i += space) {
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(0, i);
        ctx.lineTo(i, h);
        ctx.moveTo(offsetWidth, i);
        ctx.lineTo(offsetWidth - i, h);
        ctx.stroke();
        ctx.closePath();
    }
}