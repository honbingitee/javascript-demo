/*
 * @Author: hongbin
 * @Date: 2021-12-06 18:35:26
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-06 18:38:58
 * @Description:canvas 常用函数
 */

/**
 * @description: 垂直水平网格线
 * @param {CanvasRenderingContext2D} ctx canvasContext
 * @param {number} w 宽度
 * @param {number} h 高度
 * @param {number} wSpace x轴间距
 * @param {number} ySpace y轴间距
 * @return {void}
 */
function gridLine(ctx, w, h, wSpace = 100, ySpace = 100) {
    for (let i = 0; i < w; i += wSpace) {
        ctx.beginPath();
        ctx.moveTo(i, 0);
        ctx.lineTo(i, h);
        ctx.strokeStyle = "#ddd";
        ctx.stroke();
        ctx.closePath();
    }

    for (let i = 0; i < h; i += ySpace) {
        ctx.beginPath();
        ctx.moveTo(0, i);
        ctx.lineTo(w, i);
        ctx.strokeStyle = "#ddd";
        ctx.stroke();
        ctx.closePath();
    }
}

/**
 * @description: 科技风线条
 * @param {CanvasRenderingContext2D} ctx canvasContext
 * @param {number} h 高度
 * @param {number} space 间距
 * @param {string} lineColor 线条颜色
 * @return {void} 
 */
function technologyLine(ctx, h, space, lineColor = "#ccc") {
    for (let i = 0; i < h; i += space) {
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(0, i);
        ctx.lineTo(i, h);
        ctx.moveTo(offsetWidth, i);
        ctx.lineTo(offsetWidth - i, h);
        ctx.stroke();
        ctx.closePath();
    }
}