
function toggleSelected(e) {
    const container = e.parentElement;
    const product_size_select = container.querySelector(".product_size_select");
    product_size_select.setAttribute("select", 'true');
    container.setAttribute("select", 'true');
}